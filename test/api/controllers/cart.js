/* jshint node: true */
/**
 * Super Unit Testing 
 *
 * All the units for the cart controller.
 *
 * @author  Domenico Rutigliano domenico@beyondai.com.au
 * 
 */


var should = require('should');
var request = require('supertest');
var server = require('../../../app');

describe('controllers', function () {

  describe('cart', function () {
    describe('GET /cart', function () {
      it('should return a default json', function (done) {
        request(server)
          .get('/cart')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function (err, res) {
            should.not.exist(err);

            res.body.should.eql({
              "total": 0,
              "orderItems": []
            });

            done();
          });
      });
    });
    describe('POST /cart', function () {
      it('should return a default json', function (done) {
        request(server)
          .post('/cart')
          .send({
            "orderItems": [
              {
                "code": "VS",
                "count": 18
              }
            ]
          })
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function (err, res) {
            should.not.exist(err);

            res.body.should.eql({
              "total": 3396,
              "orderItems": [
                {
                  "code": "VS",
                  "package": 3,
                  "count": 1,
                  "price": 699
                },
                {
                  "code": "VS",
                  "package": 5,
                  "count": 3,
                  "price": 2697
                }
              ]
            });

            done();
          });
      });
    });
    describe('DELETE /cart', function () {
      it('should return a default json', function (done) {
        request(server)
          .delete('/cartVS')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function (err, res) {
            should.not.exist(err);

            res.body.should.eql({
              "success": 1,
              "description": "Product correctly removed form the cart!"
            });

            done();
          });
      });
    });
  });

});