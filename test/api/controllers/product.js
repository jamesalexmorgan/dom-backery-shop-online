/* jshint node: true */
/**
 * Super Unit Testing 
 *
 * All the units for the product controller.
 *
 * @author  Domenico Rutigliano domenico@beyondai.com.au
 * 
 */

var should = require('should');
var request = require('supertest');
var server = require('../../../app');

describe('controllers', function () {

  describe('product', function () {
    describe('POST /product', function () {
      it('should accept a object in the body', function (done) {

        request(server)
          .post('/product')
          .send({
            "name": "Cornetti alla Crema",
            "code": "CC",
            "packagingOptions": [
              {
                "count": 3,
                "price": 595
              },
              {
                "count": 5,
                "price": 995
              },
              {
                "count": 9,
                "price": 1695
              }
            ]
          })
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function (err, res) {
            should.not.exist(err);

            res.body.should.eql({
              "success": 1,
              "description": "Product added to the list!"
            });

            done();
          });
      });
    });
    describe('GET /product', function () {

      it('should return a default json', function (done) {

        request(server)
          .get('/product')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function (err, res) {
            should.not.exist(err);
            res.body.should.eql({
              "products": [
                {
                  "name": "Vegemite Scroll",
                  "code": "VS",
                  "packagingOptions": [
                    {
                      "count": 3,
                      "price": 699
                    },
                    {
                      "count": 5,
                      "price": 899
                    }
                  ]
                },
                {
                  "name": "Blueberry Muffin",
                  "code": "BM",
                  "packagingOptions": [
                    {
                      "count": 2,
                      "price": 995
                    },
                    {
                      "count": 5,
                      "price": 1695
                    },
                    {
                      "count": 8,
                      "price": 2495
                    }
                  ]
                },
                {
                  "name": "Cornetti alla Crema",
                  "code": "CC",
                  "packagingOptions": [
                    {
                      "count": 3,
                      "price": 595
                    },
                    {
                      "count": 5,
                      "price": 995
                    },
                    {
                      "count": 9,
                      "price": 1695
                    }
                  ]
                }
              ]
            });
            done();
          });
      });

    });
    describe('GET /product/VS', function () {

      it('should return a default json', function (done) {

        request(server)
          .get('/product/VS')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function (err, res) {
            should.not.exist(err);
            res.body.should.eql({
              "name": "Vegemite Scroll",
              "code": "VS",
              "packagingOptions": [
                {
                  "count": 3,
                  "price": 699
                },
                {
                  "count": 5,
                  "price": 899
                }
              ]
            });
            done();
          });
      });

    });
    describe('PUT /product/VS', function () {

      it('should return a default json', function (done) {

        request(server)
          .put('/product/VS')
          .send({
            "name": "Vegemite Roll",
            "code": "VR",
            "packagingOptions": [
              {
                "count": 4,
                "price": 799
              },
              {
                "count": 6,
                "price": 999
              }
            ]
          })
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function (err, res) {
            should.not.exist(err);
            res.body.should.eql({
              "success": 1,
              "description": "Product updated!"
            });
            done();
          });
      });

    });
    describe('DELETE /product/BM', function () {

      it('should return a default json', function (done) {

        request(server)
          .delete('/product/BM')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function (err, res) {
            should.not.exist(err);
            res.body.should.eql({
              "success": 1,
              "description": "Product deleted!"
            });
            done();
          });
      });

    });
  });

});
