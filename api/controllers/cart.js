/* jshint node: true */
/**
 * Amazing Cart Controller 
 *
 * All the endpoint for the cart controller.
 *
 * @author  Domenico Rutigliano domenico@beyondai.com.au
 * 
 */

'use strict';
// Include our "db"
var db = require('../db/db.js')();

// Exports all the functions to perform on the db
module.exports = { addToCart, getCart, delCart };

//GET /cart retrive the cart
function getCart(req, res, next) {
    res.json(db.cart());
}
//POST /cart add to the cart
function addToCart(req, res, next) {
    var cartItems = req.body.orderItems;
    var cart = db.addToCart(cartItems);
    //console.dir(cart, { depth: null, colors: true });
    if (cart) {
        res.json(cart);
    } else {
        res.status(204).send();
    }
}
//DELETE /cart/{code} remove from the cart
function delCart(req, res, next) {
    var code = req.swagger.params.code.value; //req.swagger contains the path parameters
    if (typeof next != 'function') {
        db.removeFromCart(code);
    } else {
        if (db.removeFromCart(code)) {
            res.json({ success: 1, description: "Product correctly removed form the cart!" });
        } else {
            res.status(204).send();
        }
    }


}


