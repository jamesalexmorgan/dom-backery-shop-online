/* jshint node: true */
/**
 * Amazing Prouct Controller  
 *
 * All the endpoints for the product controller.
 *
 * @author  Domenico Rutigliano domenico@beyondai.com.au
 * 
 */

'use strict';
// Include our "db"
var db = require('../db/db.js')();
var cart = require('../controllers/cart.js');
// Exports all the functions to perform on the db
module.exports = { getAll, save, getOne, update, delProduct };

//GET /product operationcode
function getAll(req, res, next) {
    res.json({ products: db.find() });
}
//POST /product operationcode
function save(req, res, next) {
    res.json({ success: db.save(req.body), description: "Product added to the list!" });
}
//GET /product/{code} operationcode
function getOne(req, res, next) {
    var code = req.swagger.params.code.value; //req.swagger contains the path parameters
    var product = db.find(code);
    if (product) {
        res.json(product);
    } else {
        res.status(204).send();
    }
}
//PUT /product/{code} operationcode
function update(req, res, next) {
    var code = req.swagger.params.code.value; //req.swagger contains the path parameters
    var product = req.body;
    if (db.update(code, product)) {
        res.json({ success: 1, description: "Product updated!" });
    } else {
        res.status(204).send();
    }

}
//DELETE /product/{code} operationcode
function delProduct(req, res, next) {
    var code = req.swagger.params.code.value; //req.swagger contains the path parameters
    if (db.remove(code)) {
        cart.delCart(req,res,true);
        res.json({ success: 1, description: "Product deleted!" });
    } else {
        res.status(204).send();
    }

}


