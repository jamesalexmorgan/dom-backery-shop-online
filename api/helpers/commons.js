/* jshint node: true */
/**
 * API Helpers 
 *
 * These function really do the tick isn't it ?
 *
 * @author  Domenico Rutigliano domenico@beyondai.com.au
 * 
 */

'use strict';

const _ = require('underscore');

module.exports = function () {
    return {
        productlist:[],
        /* lets loop trough to find out how to split the order number according to the package details*/
        loop(x, z) {
            var last = [], left = 0, counter = 0;
            while (x >= z) {
                last = [x, z];
                left = x - z;
                counter++;
                if (x >= z) {
                    x = x - z;
                }
            }
            if (counter == 0 || left == 1) {
                return false;
            }
            return { counter, left: left };
        },
        /* tyding up the results object */
        postprocess(results) {
            var _postprocess = [];
            _.each(results, function (item, index) {
                if (item) {
                    item.package = index;
                    delete item.left;
                }
                _postprocess.push(item);
            });
            return _postprocess;
        },
        /* Looping trough the packages to process the details and return the order split in packages */
        PackageLoop(x, results) {
            var cts = this;
            var res = [];
            var subObj = {};
            this.productlist.forEach(function (item) {

                var z = item.count;
                res = cts.loop(x, z);
                if (res)
                    results[z] = res;
                if (results[z]) {
                    var left = results[z].left;
                    if (left > 0) {
                        subObj = cts.PackageLoop(results[z].left, {});

                        if (Object.entries(subObj).length !== 0) {
                            delete results[z][z + "-" + left];
                            Object.keys(subObj).forEach(function (subItemkey) {
                                subObj[subItemkey].package = subItemkey;
                                delete subObj[subItemkey].left;
                                subObj = subObj[subItemkey];
                            });
                            results[z].subpackage = subObj;
                        } else {
                            delete results[z];
                        }


                    }
                }
            });
            return results;
        }

    };
};