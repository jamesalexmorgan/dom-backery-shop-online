/* jshint node: true */
/**
 * DB Controller
 *
 * Initialize a mock database module with  arrays of objects.
 *
 * @author  Domenico Rutigliano domenico@beyondai.com.au
 * 
 */

'use strict';


var _ = require('underscore');

/* Loading the helpers lib */
var helpers = require('../helpers/commons.js')();

/* seeding the "DB" with the json object */
var seed = require('./seeds/db.json');

module.exports = function () {
    return {
        productList: seed,
        orderItems: [],
        cartTotal: 0,
        /*
         * Save the product inscodee the "db".
         */
        save(product) {
            this.productList.push(product);
            return 1;
        },
        /*
         * Retrieve a product with a given code or return all the products if the code is undefined.
         */
        find(code) {
            if (code) {
                return this.productList.find(function (element) {
                    return element.code === code;
                });
            } else {
                return this.productList;
            }
        },
        /*
         * Delete a product with the given code.
         */
        remove(code) {
            var found = 0;
            this.productList = this.productList.filter(function (element) {
                if (element.code === code) {
                    found = 1;
                } else {
                    return element.code !== code;
                }
            });
            return found;
        },
        /*
         * Update a product with the given code
         */
        update(code, product) {
            var productIndex = this.productList.findIndex(function (element) {
                return element.code === code;
            });
            if (productIndex !== -1) {
                this.productList[productIndex].name = product.name;
                this.productList[productIndex].code = product.code;
                this.productList[productIndex].packagingOptions = product.packagingOptions;
                return 1;
            } else {
                return 0;
            }
        },
        cart() {
            return { total: this.cartTotal, orderItems: this.orderItems };
        },
        removeFromCart(code) {
            var tmpCart = [];
            var flag = false;
            this.orderItems.forEach(function (item) {
                if (item.code === code) {
                    flag = true;
                } else {
                    tmpCart.push(item);
                }
            });
            this.orderItems = tmpCart;
            return flag;
        },
        addToCart(cartItems) {
            var cts = this;
            var total = this.cartTotal;

            _.each(cartItems, function (cartItem, key) {

                var product = cts.find(cartItem.code);
                if (product) {
                    helpers.productlist = product.packagingOptions;
                    var bundles = {};
                    /* if the biggest bundle are the chepest per item I am ordeing the results by lowest counter for the bigger bundle so that I pick always the cheaper combination */
                    bundles = helpers.postprocess(helpers.PackageLoop(cartItem.count, {})).sort(function (count1, count2) {
                        if (count1.counter < count2.counter) return -1;
                        if (count1.counter > count2.counter) return 1;
                    })[0];
                    if (bundles) {
                        if (bundles.subpackage) {
                            helpers.productlist.forEach(function (prices) {
                                if (bundles.package == prices.count) {
                                    total += prices.price * bundles.counter;
                                    cts.orderItems.push({ code: cartItem.code, package: parseInt(bundles.package), count: bundles.counter, price: prices.price * bundles.counter });
                                }
                                if (bundles.subpackage.package == prices.count) {
                                    total += prices.price * bundles.subpackage.counter;
                                    cts.orderItems.push({ code: cartItem.code, package: parseInt(bundles.subpackage.package), count: bundles.subpackage.counter, price: prices.price * bundles.subpackage.counter });
                                }
                            });
                        } else {
                            return false;
                        }
                    }

                }
            });
            this.cartTotal = total;
            this.orderItems = cts.orderItems;

            return { total: this.cartTotal, orderItems: this.orderItems };
        }
    };
};

