# Dom's online bakery shop

This is a simple API  built with Swagger/ExpressJS 
This small project uses the KISS methodology (Keep It Simple, Stupid)

## Installation

NOTE: This guide assumes that you are confident with Node Package Manager (NPM) and Node and that they are already installed on your machine.

Clone the project to your local machine:
```bash
git clone https://gitlab.com/rutigliano/dom-backery-shop-online.git
```
Use node package manager [npm](https://www.npmjs.com/) to install all the dependencies.

```bash
npm install 
```

## Usage (Start the Documentation)

This project runs its Documentation on  Swagger Interface where it is possible to execute the endpoints with sample payloads and gets responses from the Server which must be up.
Swagger contains all the required documentation and descriptions of the endpoints.
Execute the following command to run it. 
NOTE: run all the commands in the project root folder.

```bash
npm run edit
```

## Usage (Start the Server)

Once Swagger interface is running start the server to test the endpoints in another terminal tab. 

Execute:
```bash
npm run start
```

## Run the unit testings
NOTE: If the server is running, the unit test will fail. Terminate the Server before running the unit testing. 
Execute:
```bash
npm run test
```
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)